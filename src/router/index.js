import Vue from 'vue'
import Router from 'vue-router'
// import Blog from '@/page/blog'
// import Life from '@/page/life'
import Back from '@/page/back'
import Login from '@/page/login'
import Index from '@/page/index'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Index
    },
    // {
    //   path: '/blog',
    //   component: Blog
    // },
    // {
    //   path: '/life',
    //   component: Life
    // },
    {
      path: '/back',
      component: Back
    },
    {
      path: '/login',
      component: Login
    }
  ]
})
